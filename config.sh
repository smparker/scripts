#!/bin/bash

function readlink { python -c "from __future__ import print_function; import os.path; print(os.path.realpath('$1'))"; }


scriptdir="$( dirname $(readlink $0) )"

export PATH=$scriptdir/bin:$PATH
export PYTHONPATH=$scriptdir/python:$PYTHONPATH

