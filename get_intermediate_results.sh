#!/bin/bash

nodefile=""
dirfile=""

for x in $(ls SLURM_WORK_NODE* 2> /dev/null); do
  if [ "$nodefile" == "" ]; then
    nodefile="$x"
  else
    echo "More than one slurm node file found!"
    exit
  fi
done

for x in $(ls SLURM_WORK_DIR* 2> /dev/null); do
  if [ "$dirfile" == "" ]; then
    dirfile="$x"
  else
    echo "More than one slurm dir file found!"
    exit
  fi
done

if [ "$nodefile" == "" ] || [ "$dirfile" == "" ]; then
  echo "Found no SLURM files with WORK_NODE and WORK_DIR info"
  exit
fi

node=$(cat $nodefile)
dir=$(cat $dirfile)

ssh $node "cd $dir && tmd collect" > surfaces.out
#ssh $node "module load turbomole && cd $dir && log2egy" > egy.log
