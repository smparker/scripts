#!/bin/bash

usage() {
  echo 'Organizes the contents of the repo by arranging:'
  echo '  - symlinks for executable scripts into bin'
  echo '  - symlinks for python modules into python'
  echo
  echo 'usage:'
  echo "  $(basename $0) [options]"
  echo
  printf "%-20s %60s\n" "-u" "add --user command to python installs"
}

# update these variables to register new scripts/modules
executables="
  bagel/x2b.py
  chem/x2c.py
  chem/join-xyz.py
  chem/dist-xyz.py
  chem/bend-xyz.py
  chem/tors-xyz.py
  chem/puck-xyz.py
  chem/outp-xyz.py
  chem/cut-bond.py
  dalton/xyz2dal.py
  math/fd.py
  math/bootstrap.py
  math/broaden.py
  math/interpolate.py
  qchem/q2x.sh
  qchem/x2q.sh
  qchem/qconv.sh
  qchem/qscan.sh
  tm/occupiedonly.sh
  tm/coordrestore.sh
  tm/t2c.py
  tm/t2super.py
  util/process_bib.py
"

pythonmodules="
  chem/xyz.py
  math/fd.py
  math/bootstrap.py
  math/broaden.py
  math/interpolate.py
  math/fithelper.py
"

pythonpackages="
"

if [ "$1" == "-h" ] || [ "$1" == "--help" ]; then
  usage
  exit
fi

if [ "$1" == "-u" ]; then
  user="--user"
fi

mkdir -p bin python

for x in $executables; do
  src="../src/$x"
  dest="bin/$(basename ${x%.*})"
  if [ ! -f $dest ]; then
    echo "linking $x to $dest"
    ln -s $src $dest
  fi
done

for x in $pythonmodules; do
  src="../src/$x"
  dest="python/$(basename $x)"
  if [ ! -f $dest ]; then
    echo "linking $x to $dest"
    ln -s $src $dest
  fi
done

thisdir=$PWD
for x in $pythonpackages; do
  cd src/$x
    python -m pip install $user -e .
  cd $thisdir
done
