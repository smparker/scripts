#!/usr/bin/env python3

from __future__ import print_function

import math
import numpy as np
import argparse

import subprocess

BOHR2ANG = 0.529177210903

def read_coord():
    cc = subprocess.run(["sdg", "-c", "-b", "coord"], capture_output=True, encoding='UTF-8').stdout
    cclines_raw = cc.split("\n")
    cclines = [ l for l in cclines_raw if l != "" ]
    natom = len(cclines)

    symbols = []
    coords = np.zeros([natom, 3])

    for iatom in range(natom):
        tmp = cclines[iatom].split()

        symb = str(tmp[3])
        symbols.append(symb)

        coords[iatom,:] = [ float(x) for x in tmp[0:3] ]

    coords *= BOHR2ANG
    return symbols, coords

parser = argparse.ArgumentParser(description="Write simplified CIF file from turbomole directory")

args = parser.parse_args()

symbols, coords = read_coord()

cl = subprocess.run(["sdg", "-t", "cell"], capture_output=True, encoding='UTF-8').stdout
angstrom = "angs" in cl

cellline = subprocess.run(["sdg", "-c", "-b", "cell"], capture_output=True, encoding='UTF-8').stdout

cellparams = [ float(x) for x in cellline.split() ]
if len(cellparams) == 6: # 3D
  a, b, c, alpha_angle, beta_angle, gamma_angle = cellparams
elif len(cellparams) == 3: # 2D
  a, b, gamma_angle = cellparams

  c = 10
  alpha_angle = 90
  beta_angle = 90
else: #
  raise Exception("only 3D and 2D structures currently implemented")

if not angstrom:
    a *= BOHR2ANG
    b *= BOHR2ANG
    c *= BOHR2ANG

alpha, beta, gamma = np.radians(alpha_angle), np.radians(beta_angle), np.radians(gamma_angle)
cosa, cosb, cosg = np.cos(alpha), np.cos(beta), np.cos(gamma)
Omega = a * b * c * np.sqrt(1.0 - cosa**2 - cosb**2 - cosg**2 + 2.0 * cosa * cosb * cosg)
vecs = np.array( [ [ a, 0.0, 0.0 ],
                   [ b * cosg, b * np.sin(gamma), 0.0 ],
                   [ c * cosb, c * (cosa - cosb * cosg)/np.sin(gamma),
                       Omega / (a * b * np.sin(gamma)) ] ]).T

cart2frac = np.linalg.inv(vecs)
fracs = np.einsum("fc,ac->af", cart2frac, coords)

flat = fracs.reshape(-1)
for i in range(flat.size):
    while flat[i] <= 0.0:
        flat[i] += 1.0
    while flat[i] >= 1.0:
        flat[i] -= 1.0

print("data_global _chemical_name \'{0}\'".format("turbomole"))
print("_cell_length_a {}".format(a))
print("_cell_length_b {}".format(b))
print("_cell_length_c {}".format(c))
print("_cell_angle_alpha {}".format(alpha_angle))
print("_cell_angle_beta  {}".format(beta_angle))
print("_cell_angle_gamma {}".format(gamma_angle))
print("_symmetry_space_group_name_H-M \'P 1\'")
print("loop_")
print("_atom_site_label")
print("_atom_site_fract_x")
print("_atom_site_fract_y")
print("_atom_site_fract_z")
for i, s in enumerate(symbols):
    x, y, z = fracs[i,:]
    print("{:3s} {:16.12f} {:16.12f} {:16.12f}".format(s, x, y, z))
