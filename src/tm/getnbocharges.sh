#!/usr/bin/bash

awk 'BEGIN {nat=0; dash=0}
     /^\s*Natural Population/ {nat=1}
     /\* Total \*/ {nat=0}
     {
       if (nat>0) {
         if (/^\s*-+/) {
           dash = (1 - dash)
         }
         else if (dash == 1) {
           print
         }
       }
     }' $1
