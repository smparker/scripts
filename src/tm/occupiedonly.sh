#!/bin/bash

if [ "$(which sdg)" == "" ]; then
  echo "sdg is required! load TURBODIR/scripts!"
  exit 1;
fi

if [ ! -f "control" ]; then
  echo "Control file not found!"
  exit 1
fi

remove_unoccupied_irrep() {
  mo=$1
  b=$2

  if [ ! -f $mo ]; then
    echo Expected mos in "$mo" but file not found
    exit 1
  fi

  irrep=$(echo $b | awk '{ split($0,a,":"); print a[1] }')
  bound=$(echo $b | awk '{ split($0,a,":"); print a[2] }')

  awk -v irr=$irrep -v b=$bound '
    BEGIN { keep = "yes" }
    /^ *[0-9]+ +[a-z0-9]+ +eigenvalue=[-+.0-9]+D[-+][0-9]+ +nsaos=[0-9]+ *$/ {
      if ($2 == irr && $1 > b) {
        keep = "no"
      }
      else {
        keep="yes"
      }
    }
    /^\$/ { keep = "yes" }
    {
      if (keep == "yes") {
        print
      }
    }' $mo > ${mo}.tmp
  cp $mo ${mo}.bk
  mv ${mo}.tmp $mo
}

uhf=$(sdg uhf 2>/dev/null)

if [ "$uhf" == "" ]; then
  # search for scfmo
  filecheck=$( awk ' /\$scfmo +file=[a-zA-Z]+/ { split($2,a,"="); print a[2] }' control )
  if [ "$filecheck" == "" ]; then
    mofiles="control"
  else
    mofiles="$filecheck"
  fi
  bounds=$(sdg -b 'closed shells' | awk '{ split($2,range,"-"); printf "%s:%d\n", $1, range[2] }')

  for b in $bounds; do
    remove_unoccupied_irrep $mofiles $b
  done
else
  # search for uhfmo_alpha
  alphacheck=$( awk ' /\$uhfmo_alpha +file=[a-zA-Z]+/ { split($2,a,"="); print a[2] }' control )
  # search for uhfmo_beta
  betacheck=$( awk ' /\$uhfmo_beta +file=[a-zA-Z]+/ { split($2,a,"="); print a[2] }' control )

  if [ "alphacheck" == "" ]; then
    alphamo="control"
  else
    alphamo="$alphacheck"
  fi
  if [ "betacheck" == "" ]; then
    betamo="control"
  else
    betamo="$betacheck"
  fi
  if [ "$alphamo" == "$betamo" ]; then
    echo occupied_only only works when alpha and beta orbitals are in separate files
    echo but, both are found in file $alphamo
    exit 1
  fi

  alphabounds=$(sdg -b 'alpha shells' | awk '{ split($2,range,"-"); printf "%s:%d\n", $1, range[2] }')
  betabounds=$(sdg -b 'beta shells' | awk '{ split($2,range,"-"); printf "%s:%d\n", $1, range[2] }')

  for b in $alphabounds; do
    remove_unoccupied_irrep $alphamo $b
  done
  for b in $betabounds; do
    remove_unoccupied_irrep $betamo $b
  done
fi
