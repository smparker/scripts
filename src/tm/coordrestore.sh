#!/bin/bash

# Restores the coordinate file to the specified previous step (read from gradient file)
# by default, writes that coordinate onto $coord
usage() {
  echo "Restores a previous steps geomemtry to the coord file"
  echo
  echo "usage:"
  echo "  $(basename $0) <cycle number>"
}

if [ ! -f "gradient" ]; then
  echo "gradient file not found!"
  usage
  exit 1
fi

if [ ! -f "coord" ]; then
  echo "coord file not found!"
  usage
  exit 1
fi

cyc=$1

if [ "$cyc" == "" ]; then
  usage
  exit 1
fi

# check that cycle is found in file
found=$( awk -v cyc=$cyc '/cycle *= *([0-9]+)/ { tmp = gensub("="," ", $0); split(tmp, a); if (cyc == a[2]) { print "ok" } }' gradient )

if [ "$found" != "ok" ]; then
  echo "Provided cycle not found in file gradient"
  exit 1
fi

# print coordinates from given cycle
echo '$coord' > coord.tmp
awk -v cyc=$cyc '
  /cycle *= *([0-9]+)/ {
    tmp = gensub("="," ", $0);
    split(tmp, a);
    if (cyc == a[2]) {
      found = 2;
    } else {
      found = 0;
    }
  }

  {
    if (found == 2) {
      found = found - 1;
    }
    if (found == 1 && / *[0-9\-+.D]+ *[0-9\-+.D]+ *[0-9\-+.D]+ *[a-z]+/) {
      print;
    }
  }' gradient >> coord.tmp

# add non "coord" parts of coord file onto coord.tmp
awk '
  {
    if (/^\$coord/) { mute = 1 }
    else if (/^\$/) { mute = 0 }

    if (mute == 0) { print }
  }' coord >> coord.tmp

mv coord.tmp coord
