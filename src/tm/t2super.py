#!/usr/bin/env python3

from __future__ import print_function

import math
import numpy as np
import argparse

import subprocess

BOHR2ANG = 0.529177210903

def read_coord():
    cc = subprocess.run(["sdg", "-c", "-b", "coord"], capture_output=True, encoding='UTF-8').stdout
    cclines_raw = cc.split("\n")
    cclines = [ l for l in cclines_raw if l != "" ]
    natom = len(cclines)

    symbols = []
    coords = np.zeros([natom, 3])

    for iatom in range(natom):
        tmp = cclines[iatom].split()

        symb = str(tmp[3])
        symbols.append(symb)

        coords[iatom,:] = [ float(x) for x in tmp[0:3] ]

    return symbols, coords

parser = argparse.ArgumentParser(description="Generate a supercell from a turbomole directory")

parser.add_argument("images", nargs=3, type=int, default=[1, 1, 1], help="number of images in each direction")

args = parser.parse_args()

symbols, coords = read_coord()

cellline = subprocess.run(["sdg", "-c", "-b", "cell"], capture_output=True, encoding='UTF-8').stdout

a, b, c, alpha_angle, beta_angle, gamma_angle = [ float(x) for x in cellline.split() ]
alpha, beta, gamma = np.radians(alpha_angle), np.radians(beta_angle), np.radians(gamma_angle)
cosa, cosb, cosg = np.cos(alpha), np.cos(beta), np.cos(gamma)
Omega = a * b * c * np.sqrt(1.0 - cosa**2 - cosb**2 - cosg**2 + 2.0 * cosa * cosb * cosg)
vecs = np.array( [ [ a, 0.0, 0.0 ],
                   [ b * cosg, b * np.sin(gamma), 0.0 ],
                   [ c * cosb, c * (cosa - cosb * cosg)/np.sin(gamma),
                       Omega / (a * b * np.sin(gamma)) ] ]).T
def ranges(x):
    return (-x//2+1, x//2+1)

with open("super.coord", "w") as f:
    print("$coord", file=f)
    for i in range(*ranges(args.images[0])):
        for j in range(*ranges(args.images[1])):
            for k in range(*ranges(args.images[2])):
                cc = coords + i * vecs[0,:] + j * vecs[1,:] + k * vecs[2,:]
                for ia in range(len(symbols)):
                    print("{:22.12f} {:22.12f} {:22.12f} {:8s}".format(cc[ia,0], cc[ia,1], cc[ia,2], symbols[ia]), file=f)
    print("$end", file=f)

with open("super.cell", "w") as f:
    print("$cell", file=f)
    print("   {:22.12f} {:22.12f} {:22.12f} {:22.12f} {:22.12f} {:22.12f}".format(a*args.images[0], b * args.images[1], c* args.images[2], alpha_angle, beta_angle, gamma_angle), file=f)
