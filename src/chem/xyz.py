#!/usr/bin/env python

from __future__ import print_function

import numpy as np

class Geometry(object):
    """Coordinates and symbols from xyz"""
    def __init__(self, symbols, coords):
        self.symbols = symbols
        self.coord = coords
        self.natoms = len(symbols)

    def bonds(self):
        out = []
        for i in range(self.natoms):
            for j in range(i):
                vec = self.coord[i,:] - self.coord[j,:]
                dist = np.linalg.norm(vec)
                if are_bonded(self.symbols[i], self.symbols[j], dist):
                    out.append((i,j))
        return out

    def print(self):
        natoms = len(self.symbols)
        print("%d" % natoms)
        print()
        for i in range(natoms):
            print("%3s %22.16f %22.16f %22.16f" %
                (self.symbols[i], self.coord[i,0], self.coord[i,1], self.coord[i,2]))

    @classmethod
    def from_xyz(cls, filename):
        """generate structure from .xyz file"""
        symbols = []
        fh = open(filename,"r")

        natoms = int(fh.readline())
        comment = fh.readline()

        coords = np.zeros([natoms,3])

        for iatom in range(natoms):
            tmp = fh.readline().split()

            symb = str(tmp[0])
            symbols.append(symb)

            coords[iatom,:] = [ float(x) for x in tmp[1:4] ]

        return cls(symbols, coords)
