#!/usr/bin/env python

import math
import numpy as np

import argparse

parser = argparse.ArgumentParser(description="compute angle from xyz file")
parser.add_argument("file",  type=str, help="xyz molecule file")
parser.add_argument("atoms", type=int, nargs=3, help="atom numbers to compute bend in xyz file")

args = parser.parse_args()

with open(args.file) as f:
    raw = f.readlines()

atoms = np.zeros([3,3])
for i in range(3):
  for j in range(3):
    atoms[i,j] = float(raw[args.atoms[i]+1].split()[j+1])

b1 = atoms[0] - atoms[1]
b2 = atoms[2] - atoms[1]

b1 /= np.linalg.norm(b1)
b2 /= np.linalg.norm(b2)

phi = math.acos( np.dot(b1,b2) )

print(math.degrees(phi))
