#!/usr/bin/env python

from __future__ import print_function

import networkx as nx
import numpy as np

covalent = { 'h' : 0.31, 'c' : 0.76, 'n' : 0.71, 'o' : 0.66, 'ni' : 1.24 }

def are_bonded(isym, jsym, dist):
    thr = 1.2 * (covalent[isym.lower()] + covalent[jsym.lower()])
    return dist < thr;

# X-H distances for adding caps
h_dist = { 'c' : 1.08 }

class Structure(object):
    """Coordinates and symbols from xyz"""
    def __init__(self, symbols, coords):
        self.symbols = symbols
        self.coord = coords
        self.natoms = len(symbols)

    def bonds(self):
        out = []
        for i in range(self.natoms):
            for j in range(i):
                vec = self.coord[i,:] - self.coord[j,:]
                dist = np.linalg.norm(vec)
                if are_bonded(self.symbols[i], self.symbols[j], dist):
                    out.append((i,j))
        return out

    def print(self):
        natoms = len(self.symbols)
        print("%d" % natoms)
        print()
        for i in range(natoms):
            print("%3s %22.16f %22.16f %22.16f" %
                (self.symbols[i], self.coord[i,0], self.coord[i,1], self.coord[i,2]))

    @classmethod
    def from_xyz(cls, filename):
        """generate structure from .xyz file"""
        symbols = []
        fh = open(filename,"r")

        natoms = int(fh.readline())
        comment = fh.readline()

        coords = np.zeros([natoms,3])

        for iatom in range(natoms):
            tmp = fh.readline().split()

            symb = str(tmp[0])
            symbols.append(symb)

            coords[iatom,:] = [ float(x) for x in tmp[1:4] ]

        return cls(symbols, coords)

def main():
    import argparse

    ap = argparse.ArgumentParser(description="Cuts specified bonds in an xyz file and replaces them with" +
            "properly oriented and spaced leaving groups")

    ap.add_argument("molecule", help="path to xyz file")
    ap.add_argument("cut", nargs='*', help="list of pairs of atoms between which to cut bonds")
    ap.add_argument('-l', '--leave', choices=("H"), default="H", help="leaving group")

    args = ap.parse_args()

    molecule = Structure.from_xyz(args.molecule)

    graph = nx.Graph()
    for iat in range(molecule.natoms):
        graph.add_node(iat)

    for bond in molecule.bonds():
        graph.add_edge(*bond)

    to_cut = args.cut
    if len(to_cut)%2 != 0:
        print("Bonds must be specified with pairs of atoms. Even number of atoms must be specified")
        raise

    to_cut = [ (int(to_cut[i])-1,int(to_cut[i+1])-1) for i in range(0, len(to_cut), 2) ]
    for b in to_cut:
        graph.remove_edge(*b)

    mol_frags = [ ]
    fragments = nx.connected_components(graph)
    for f in fragments:
        cuts = [ x for x in to_cut if x[0] in f or x[1] in f ]
        natoms = len(f) + len(cuts)
        coord = np.zeros([natoms,3])
        symbols = []

        # copy over existing atoms
        for iat, jat in enumerate(f):
            coord[iat,:] = molecule.coord[jat,:]
            symbols.append(molecule.symbols[jat])

        # add caps
        for kat, (i, j) in enumerate(cuts):
            if i in f == j in f:
                print("One index should be in frag, one should be out. What's going on?")
                raise

            # set so i is in fragment, j is the partner that was cut
            if j in f:
                i, j = j, i

            # get unit vector in direction of missing atom
            vec = molecule.coord[j,:] - molecule.coord[i,:]
            vec /= np.linalg.norm(vec)

            dist = h_dist[molecule.symbols[i].lower()]
            coord[kat+len(f),:] = molecule.coord[i,:] + dist * vec
            symbols.append('H')


        mol_frags.append(Structure(symbols, coord))

    for m in mol_frags:
        m.print()


if __name__ == "__main__":
    main()
