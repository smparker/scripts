#!/usr/bin/env python

from __future__ import print_function

import math
import numpy as np
import argparse

def read_xyz(filename):
    symbols = []
    fh = open(filename,"r")

    natoms = int(fh.readline())
    comment = fh.readline()

    coords = np.zeros([natoms,3])

    for iatom in range(natoms):
        tmp = fh.readline().split()

        symb = str(tmp[0])
        symbols.append(symb)

        coords[iatom,:] = [ float(x) for x in tmp[1:4] ]

    return symbols, coords

parser = argparse.ArgumentParser(description="Write simplified CIF file from xyz and cell parameters")
parser.add_argument("file", type=str, help="xyz file")
parser.add_argument("data", type=float, nargs='+', help='input either 6 cell parameters or 3x3 numbers for lattice vectors')
parser.add_argument("-w", "--no_wrap", action="store_true", help='do not wrap coordinates to fit into first unit cell')
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument("-c", "--cell", action='store_true', help="unit cell in terms of 6 cell parameters: a b c alpha beta gamma")
group.add_argument("-l", "--lattice", action='store_true', help="unit cell in terms of 3 lattice vectors")

args = parser.parse_args()

symbols, coords = read_xyz(args.file)

a, b, c = 1.0, 1.0, 1.0
alpha_angle, beta_angle, gamma_angle = 90.0, 90.0, 90.0

print("data_global _chemical_name \'{0}\'".format(args.file))
if args.cell:
    if len(args.data) != 6:
        raise Exception("6 parameters required for cell parameters")

    a, b, c, alpha_angle, beta_angle, gamma_angle = args.data
    alpha, beta, gamma = np.radians(alpha_angle), np.radians(beta_angle), np.radians(gamma_angle)
    cosa, cosb, cosg = np.cos(alpha), np.cos(beta), np.cos(gamma)
    Omega = a * b * c * np.sqrt(1.0 - cosa**2 - cosb**2 - cosg**2 + 2.0 * cosa * cosb * cosg)
    vecs = np.array( [ [ a, 0.0, 0.0 ],
                       [ b * cosg, b * np.sin(gamma), 0.0 ],
                       [ c * cosb, c * (cosa - cosb * cosg)/np.sin(gamma),
                           Omega / (a * b * np.sin(gamma)) ] ]).T

    cart2frac = np.linalg.inv(vecs)
    fracs = np.einsum("fc,ac->af", cart2frac, coords)

    flat = fracs.reshape(-1)
    for i in range(flat.size):
        while flat[i] <= 0.0:
            flat[i] += 1.0
        while flat[i] >= 1.0:
            flat[i] -= 1.0

else:
    if len(args.data) != 9:
        raise Exception("9 parameters required for lattice parameters")

    latt = np.array(args.data).reshape([3,3])
    cart2frac = np.linalg.inv(latt.T)
    fracs = np.einsum("fc,ac->af", cart2frac, coords)

    a = np.linalg.norm(latt[0,:])
    b = np.linalg.norm(latt[1,:])
    c = np.linalg.norm(latt[2,:])

    ua = latt[0,:] / a
    ub = latt[1,:] / b
    uc = latt[2,:] / c

    alpha = np.arccos(np.dot(ub, uc))
    beta = np.arccos(np.dot(ua, uc))
    gamma = np.arccos(np.dot(ua, ub))

    alpha_angle, beta_angle, gamma_angle = np.degrees(alpha), np.degrees(beta), np.degrees(gamma)

if not args.no_wrap:
    flat = fracs.reshape(-1)
    for i in range(flat.size):
        while flat[i] <= 0.0:
            flat[i] += 1.0
        while flat[i] >= 1.0:
            flat[i] -= 1.0


print("_cell_length_a {}".format(a))
print("_cell_length_b {}".format(b))
print("_cell_length_c {}".format(c))
print("_cell_angle_alpha {}".format(alpha_angle))
print("_cell_angle_beta  {}".format(beta_angle))
print("_cell_angle_gamma {}".format(gamma_angle))
print("_symmetry_space_group_name_H-M \'P 1\'")
print("loop_")
print("_atom_site_label")
print("_atom_site_fract_x")
print("_atom_site_fract_y")
print("_atom_site_fract_z")
for i, s in enumerate(symbols):
    x, y, z = fracs[i,:]
    print("{:3s} {:16.12f} {:16.12f} {:16.12f}".format(s, x, y, z))
