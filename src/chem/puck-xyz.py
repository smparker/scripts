#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import argparse

ap = argparse.ArgumentParser(description="Computes distance in Angstrom between two atoms in an xyz file")

ap.add_argument("file", help="xyz file to extract structure from", type=str)
ap.add_argument("atoms", nargs='+', type=int)

args = ap.parse_args()

with open(args.file) as f:
    raw = f.readlines()

    natoms = len(args.atoms)
    positions = np.zeros([natoms, 3])

    for i, iatom in enumerate(args.atoms):
        tmp = raw[iatom+1].rstrip().split()
        positions[i, :] = [ float(x) for x in tmp[1:4] ]

def compute_pucker(xyz):
    natoms = xyz.shape[0]

    # translate to center of rings
    center = np.sum(xyz, axis=0) / natoms
    xyz -= center

    Rsin = sum( [xyz[i,:] * np.sin(2.0 * np.pi * i / natoms) for i in range(natoms)] )
    Rcos = sum( [xyz[i,:] * np.cos(2.0 * np.pi * i / natoms) for i in range(natoms)] )

    n = np.cross(Rsin, Rcos)
    n /= np.linalg.norm(n)

    q = np.einsum("ar,r->a", xyz, n)

    return np.linalg.norm(q)

q = compute_pucker(positions)
print(f"{q:22.12f}")
