#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import argparse

ap = argparse.ArgumentParser(description="Computes distance in Angstrom between two atoms in an xyz file")

ap.add_argument("file", help="xyz file to extract structure from", type=str)
ap.add_argument("--plane", "-p", nargs='+', type=int)
ap.add_argument("atoms", nargs='+', type=int)

args = ap.parse_args()

if args.plane is None:
    args.plane = args.atoms

if len(args.plane) < 3:
    print("need more than 3 atoms to define a plane")

plane_atoms = np.zeros([len(args.plane), 3])
atoms = np.zeros([len(args.atoms), 3])

with open(args.file) as f:
    raw = f.readlines()

    for i, iatom in enumerate(args.atoms):
        tmp = raw[iatom+1].rstrip().split()
        atoms[i, :] = [ float(x) for x in tmp[1:4] ]

    for i, iatom in enumerate(args.plane):
        tmp = raw[iatom+1].rstrip().split()
        plane_atoms[i, :] = [ float(x) for x in tmp[1:4] ]

def compute_plane_coordinates(xyz, plane):
    """
    transform xyz coordinates so that they are
    centered around the plane atoms.

    return the norm of the direction out of the plane
    """
    natoms = xyz.shape[0]
    nplane = plane.shape[0]

    # translate to center of rings
    center = np.sum(plane, axis=0) / nplane
    xyz -= center
    plane -= center

    u, s, vt = np.linalg.svd(plane, full_matrices=False)

    p = np.dot(plane, vt.T)
    x = np.dot(xyz, vt.T)

    return np.linalg.norm(x[:,2])

outp = compute_plane_coordinates(atoms, plane_atoms)
print(f"{outp:22.12f}")
