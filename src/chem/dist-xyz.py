#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import argparse

ap = argparse.ArgumentParser(description="Computes distance in Angstrom between two atoms in an xyz file")

ap.add_argument("file", help="xyz file to extract structure from", type=str)
ap.add_argument("iatom", metavar="iatom", type=int)
ap.add_argument("jatom", metavar="jatom", type=int)

args = ap.parse_args()

atoms = [args.iatom, args.jatom]

f = open(args.file)
raw = f.readlines()
f.close()

positions = np.zeros([2,3])

distances = [ ]

while (len(raw) > 0):
  natoms = int(raw.pop(0))
  comment = raw.pop(0).rstrip().lstrip()

  for i, iatom in enumerate(atoms):
      tmp = raw[iatom-1].rstrip().split()
      for x in range(3):
        positions[i,x] = float(tmp[x+1])

  # burn off extra atoms
  for i in range(natoms):
    raw.pop(0)

  dist = np.linalg.norm(positions[1,:] - positions[0,:])
  distances.append((comment,dist))

if len(distances) <= 0:
    print("Empty file!")
elif len(distances) == 1:
    print("%22.12f" % distances[0][1])
else:
    for i, x in enumerate(distances):
        print("frame: %d, comment: \"%s\", distance: %22.12f" % (i, x[0], x[1]))
