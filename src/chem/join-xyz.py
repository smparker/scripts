#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import argparse

import xyz

parser = argparse.ArgumentParser(description="compute torsion from xyz file")
parser.add_argument("files", type=str, nargs='+', help="xyz molecules")

args = parser.parse_args()

geoms = [ xyz.Geometry.from_xyz(x) for x in args.files ]

natoms = sum([ x.natoms for x in geoms ])
symbols = sum([ x.symbols for x in geoms ], [])

coordinates = np.zeros([natoms,3])
icoord = 0
for geo in geoms :
    for i in range(geo.natoms) :
        coordinates[icoord,:] = geo.coord[i,:]
        icoord += 1

out = xyz.Geometry(symbols, coordinates)
out.print()
