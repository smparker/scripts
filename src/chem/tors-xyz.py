#!/usr/bin/env python

from __future__ import print_function

import math
import argparse
import numpy as np

parser = argparse.ArgumentParser(description="compute torsion from xyz file")
parser.add_argument("file",  type=str, help="xyz molecule file")
parser.add_argument("atoms", type=int, nargs=4, help="atom numbers to compute torsion in xyz file")

args = parser.parse_args()

with open(args.file) as f:
    raw = f.readlines()

atoms = np.zeros([4,3])
for i in range(4):
  for j in range(3):
    atoms[i,j] = float(raw[args.atoms[i]+1].split()[j+1])

b1 = atoms[1] - atoms[0]
b2 = atoms[2] - atoms[1]
b3 = atoms[3] - atoms[2]

b2norm = np.linalg.norm(b2)

phi = math.atan2( b2norm * np.dot(b1, np.cross(b2,b3)), np.dot( np.cross(b1,b2), np.cross(b2,b3) ) )

print(math.degrees(phi))
