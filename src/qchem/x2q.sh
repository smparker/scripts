#!/bin/bash

if [ $# -eq 0 ]; then
  cat << HELP

Usage:

x2q [-c charge] [-s spin] file

converts an xyz file into a qchem formatted molecule file. 

Options:

-c	Specify the charge (default: 0)
-s	Specify the spin multiplity (default: 1)

HELP
exit
fi

CHARGE=0
SPIN=1

until [ $# -eq 0 ]; do
  case $1 in
	-c) 	CHARGE=$2
		shift; shift
		;;
	-s)	SPIN=$2
		shift; shift
		;;
	*)	FILENAME=$1
		shift
		;;
  esac
done

if [ ! -f $FILENAME ]; then
  echo "File $FILENAME not found!"; exit
fi

echo "\$molecule"
echo "$CHARGE $SPIN"
awk 'NR > 2' $FILENAME
echo "\$end"
