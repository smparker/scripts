#!/bin/bash

if [ $# -eq 0 ] || [ $1 = "-h" ]; then
  cat << HELP

  Usage:

  qscan input-file stre|bend|tors ATOM1 ATOM2 [ATOM3 [ATOM4]] initial-value step-size number-of-steps
  or
  qscan stre|bend|tors ATOM1 ATOM2 [ATOM3 [ATOM4]] initial-value step-size number-of-steps input-file

  The 'input-file' can be specified first or last.

  Strips the '\$opt' section from the 'input-file' and makes one big input file with each scan
  position.

HELP
  exit
fi

while [ $# -gt 0 ]; do
  case $1 in
	"stre") COORD="STRE"
		ATOMS="$2 $3"
		shift; shift; shift
		;;
	"bend") COORD="BEND"
		ATOMS="$2 $3 $4"
		shift; shift; shift; shift
		;;
	"tors") COORD="TORS"
		ATOMS="$2 $3 $4 $5"
		shift; shift; shift; shift; shift
		;;
	*)	FILENAME=$1
		shift
		continue
		;;
  esac

  VALUE=$1
  STEP=$2
  NUM=$3
  shift; shift; shift
done

# Add constraint to input file, if it's not there already
awk 'BEGIN {skip=1} /\$opt/ { skip=0 } /\$end/ && skip==0 {skip=1; next} skip { print;}' $FILENAME
cat << CONST
\$opt
  CONSTRAINT
    $COORD $ATOMS $VALUE
  ENDCONSTRAINT
\$end
CONST

for ((i=0;i<$NUM;++i)); do
  echo @@@;echo

  VALUE=$(echo "scale=2; $VALUE + $STEP" | bc)

  cat << MOL
\$molecule
  read
\$end
MOL
  awk 'BEGIN {skip=1} /\$opt/ || /\$molecule/ { skip=0 } /\$end/ && skip==0 {skip=1; next} skip { print;}' $FILENAME
cat << CONST

\$opt
  CONSTRAINT
    $COORD $ATOMS $VALUE
  ENDCONSTRAINT
\$end

CONST
done
