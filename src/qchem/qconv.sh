#!/bin/bash

if [ $# -eq 0 ]; then
  cat << HELP

Usage:

qconv file

extracts the convergence information for a QChem optimization

HELP
exit
fi

FILENAME=$1

awk 'BEGIN { print "Cycle\tGradient\tDisplacement\tdE";print "---------------------------------------------------" }
/ Optimization Cycle: / {cycle=$3}
/ Gradient / {grad=$2}
/ Displacement / {disp=$2}
/ Energy change / {DE=$3; print cycle"\t"grad"\t"disp"\t"DE}' \
$FILENAME
