#!/bin/bash

if [ $# -eq 0 ]; then
  cat << HELP

Usage:

q2x [-c] [-s] file

extracts all of the intermediate geometries from a QChem output \"file\" and prints them
to an .xyz file.

Options:

-c	Print only the final geometry
-s	Print only the final geometries of each subcalculation

HELP
exit
fi

ALL=1; SEP=0
OPTION="a"
until [ $# -eq 0 ]; do
  case $1 in
	-c) 	OPTION="c"
		shift
		;;
	-s)	OPTION="s"
		shift
		;;
	*)	FILENAME=$1
		shift
		;;
  esac
done

if [ ! -f $FILENAME ]; then
  echo "File $FILENAME not found!"; exit
fi

NUMATOMS=$(awk 'BEGIN { read=0 } \
  /Standard Nuclear Orientation/ { read=2 } \
  /---*/ {read-=1} \
  (NF==5 && read==1) {num=$1} \
  END {print num}' \
  $FILENAME)

case $OPTION in
  "a") 
    awk "/Standard Nuclear Orientation/ {print \"$NUMATOMS\\n\"; a=2} \
      /---*/ {a-=1} \
      (NF==5 && a==1) {printf \"%2s %10s %10s %10s\\n\", \$2, \$3, \$4, \$5}" \
      $FILENAME
    ;;

  "c") 
    let temp=NUMATOMS+2
    awk "/Standard Nuclear Orientation/ {print \"$NUMATOMS\\n\"; a=2} \
      /---*/ {a-=1} (NF==5 && a==1) \
      {printf \"%2s %10s %10s %10s\\n\", \$2, \$3, \$4, \$5}" \
      $FILENAME | tail -$temp
    ;;

  "s") 
    let temp=NUMATOMS+2
    NUMJOBS=$(awk "/Job 2 of/ {print \$4}" $FILENAME)
    if [ -z $NUMJOBS ]; then
      NUMJOBS=1
    fi
    awk "NR==1, /Job 2 of $NUMJOBS/ {print}" $FILENAME \
      | awk "/Standard Nuclear Orientation/ { print \"$NUMATOMS\\n\"; a=2 } \
      /---*/ {a-=1} (NF==5 && a==1) \
      {printf \"%2s %10s %10s %10s\\n\", \$2, \$3, \$4, \$5}" \
      | tail -$temp
    for ((i=2;i<=$NUMJOBS;++i)); do
      let j=i+1
      awk "/Job $i of $NUMJOBS/, /Job $j of $NUMJOBS/ {print}" $FILENAME | \
        awk "/Standard Nuclear Orientation/ { print \"$NUMATOMS\\n\"; a=2 } \
        /---*/ {a-=1} (NF==5 && a==1) \
        {printf \"%2s %10s %10s %10s\\n\", \$2, \$3, \$4, \$5}" | \
        tail -$temp
    done
    ;;
esac
