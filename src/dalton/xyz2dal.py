#!/usr/bin/env python

import argparse as ap

def ang2bohr(angstrom):
    return angstrom / 0.52917721067

class atom:
    def __init__(self, typ, pos):
        self.typ = typ
        self.pos = pos

if __name__ == "__main__":
    parser = ap.ArgumentParser(description="Convert .xyz structures to Dalton style molecule.inp")
    parser.add_argument('filename', type=str, help="input xyz file")
    parser.add_argument('-b', '--basis', type=str, help="basis set to use", default="INSERT BASIS")

    inp = parser.parse_args()

    f = open(inp.filename)
    raw = f.readlines()

    # first line contains number of atoms
    natoms = int(raw[0])

    # dictionary of atoms sorted by symbol
    atoms = {}

    chargeslist = [ 'h', 'he', 'li', 'be', 'b', 'c', 'n', 'o', 'f', 'ne' ]
    charges = {}
    for i, a in enumerate(chargeslist):
        charges[a] = i+1

    for line in raw[2:]:
      tmp = line.rstrip().split()
      sym = tmp[0].lower()
      xyz = [ ang2bohr(float(x)) for x in tmp[1:] ]
      if sym in atoms:
          atoms[sym].append(atom(sym,xyz))
      else:
          atoms[sym] = [atom(sym,xyz)]

    assert(natoms == sum([len(atoms[x]) for x in atoms]))

    # start writing basis file
    print "BASIS"
    print inp.basis
    print "Auto-generated molecule.inp file using"
    print "the %s basis." % (inp.basis)
    print "Atomtypes=%d" % (len(atoms))
    for elem in atoms:
        print "Charge=%1.1f Atoms=%d" % (charges[elem], len(atoms[elem]))
        for i, at in enumerate(atoms[elem]):
            label = "%s%-2d" % (at.typ, i)
            print "%-6s %22.14f %22.14f %22.14f" % (label, at.pos[0], at.pos[1], at.pos[2])
