#!/usr/bin/env python

import sys
import math
import json

if (len(sys.argv) != 2):
    print "Usage:"
    print "  %s <file>" % sys.argv[0].rsplit('/')[-1]
    exit()

f = open(sys.argv[1])
raw = f.readlines()
f.close()

natoms = int(raw[0])

raw.pop(0) # natoms
raw.pop(0) # comment

if ( len(raw) != natoms ):
    exit("Improperly formatted xyz file")

geo = []
for line in raw:
    tmp = line.split()
    name = str(tmp[0])
    x = float(tmp[1])
    y = float(tmp[2])
    z = float(tmp[3])
    geo.append( { "atom" : name, "xyz" : [x, y, z] } )

#out = { "title" : "molecule",
#        "angstrom" : True,
#        "geometry" : geo,
#        "basis" : "svp",
#        "df_basis" : "svp-jkfit"
#      }

print "{ \"bagel\" : ["

print "  { \"title\" : \"molecule\", "
print "    \"angstrom\" : true,"
print "    \"basis\" : \"svp\","
print "    \"df_basis\" : \"svp-jkfit\","

print "    \"geometry\" : ["

for i, atom in enumerate(geo):
    xx, yy, zz = atom["xyz"]
    print "      { \"atom\" : \"%s\", \"xyz\" : [ %20.12f, %20.12f, %20.12f ] }%s" % (atom["atom"], xx, yy, zz, ',' if (i+1!=len(geo)) else '' )

print "    ]"
print "  }"
print "] }"

#print json.dumps(out, sort_keys=True, indent=2, separators=(',', ': '))
