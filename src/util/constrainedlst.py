#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import scipy.optimize as spo
import scipy.spatial.distance as spd
import argparse

## Standalone script to do a constrained optimization inspired by the
## Linear Synchronous Transit algorithm. It fixes a particular bond
## and minimizes to try to keep all other distances as similar as
## possible.

class Structure(object):
    """Coordinates and symbols from xyz"""
    def __init__(self, symbols, coords):
        self.symbols = symbols
        self.coord = coords

    def print(self):
        natoms = len(self.symbols)
        print("%d" % natoms)
        print()
        for i, el in enumerate(self.symbols):
            print("%3s %22.16f %22.16f %22.16f" % (el, self.coord[i,0], self.coord[i,1], self.coord[i,2]))

    @classmethod
    def from_xyz(cls, filename):
        """generate structure from .xyz file"""
        symbols = []
        fh = open(filename,"r")

        natoms = int(fh.readline())
        comment = fh.readline()

        coords = np.zeros([natoms,3])

        for iatom in range(natoms):
            tmp = fh.readline().split()

            symb = str(tmp[0])
            symbols.append(symb)

            coords[iatom,:] = [ float(x) for x in tmp[1:4] ]

        return cls(symbols, coords)

def distance_matrix(coordw):
    """compute r_ij for all atoms i < j"""
    coord = np.reshape(coordw, (coordw.size//3,3))
    return spd.pdist(coord, 'euclidean')

def transit_metric(coordw, interp_coord, interp_distance, zeta = 1.0e-6):
    """computes metric for LST"""
    coord = np.reshape(coordw, (coordw.size//3,3))

    distance = distance_matrix(coord)
    natoms = coord.shape[0]

    coord_diff = coord - interp_coord
    dist_diff = distance - interp_distance

    metric = np.sum(dist_diff**2/(interp_distance**4))
    metric += zeta * np.vdot(coord_diff, coord_diff)

    return metric

def transit_gradient(coordw, interp_coord, interp_distance, zeta = 1.0e-6):
    """computes gradient of metric for LST"""
    coord = np.reshape(coordw, (coordw.size//3,3))

    distance = distance_matrix(coord)
    natoms = coord.shape[0]

    # initialize output
    gradient = 2.0 * zeta * (coord - interp_coord.reshape(coord.shape))

    base_grad = 2.0 * (distance-interp_distance)/(distance*interp_distance**4)
    # convert to squareform
    base_grad = spd.squareform(base_grad)

    flat_grad = np.sum(base_grad, axis=1)
    for i in range(natoms):
        gradient[i,:] += coord[i,:] * flat_grad[i]

    gradient -= np.dot(base_grad, coord)
    return np.reshape(gradient, -1)

def clst(structure, constraints, options):
    """Constrained linear synchronous transit"""
    structdistance = distance_matrix(structure.coord)

    coord = np.zeros(structure.coord.shape)
    coord[:,:] = structure.coord[:,:]

    # now optimize coordinates so that the distance between atoms is preserved
    # as much as possible during the interpolation.
    result = spo.minimize(transit_metric, coord.flatten(), jac=transit_gradient,
                            args=(structure.coord,structdistance),
                            method='SLSQP', constraints=constraints, options=options)

    return Structure(structure.symbols, result.x.reshape(coord.shape))

def add_bond_constraint(iatom, jatom, distance):
    def const(co):
        coord = co.reshape((co.size//3,3))
        vec = coord[iatom,:] - coord[jatom,:]
        dist = np.linalg.norm(vec)
        return dist - distance
    return { 'type' : 'eq', 'fun' : const }

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description='Constrained LST that tries to help guess coordinates for scans',
                                 formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    # required
    ap.add_argument('file', metavar='file', type=str, help='.xyz file containing input structure')

    # optional
    ap.add_argument('-b', '--bond', nargs=3, metavar=('atom1','atom2','distance'), help='<atom1> <atom2> distance')
    ap.add_argument('-t', '--tol', metavar="tol", type=float, default=1.0e-4, help='timestep tolerance (times within this tolerance are considered the same)')
    ap.add_argument('-n', '--maxiter', metavar="maxiter", type=int, default=500, help='maximum number of iterations for LST')
    ap.add_argument('-v', '--verbose', dest="verbose", action="store_true", help='display optimization information')
    ap.set_defaults(verbose=False)

    # parse
    args = ap.parse_args()

    # package other arguments into options to pass to LST
    options = { "maxiter" : args.maxiter,
                "disp" : args.verbose
              }

    structure = Structure.from_xyz(args.file)

    iatom, jatom, dist = int(args.bond[0])-1, int(args.bond[1])-1, float(args.bond[2])
    con = add_bond_constraint(iatom, jatom, dist)

    out = clst(structure, con, options)
    out.print()
