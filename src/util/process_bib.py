#!/usr/bin/env python3

from __future__ import print_function

import string
import bibtexparser
import argparse
import sys
import os

class JournalError(Exception):
    def __init__(self, jo):
        self.value = "Unrecognized journal: " + jo

    def __str__(self):
        return repr(self.value)

journal_abbreviations = {
    "Advances in quantum chemistry" : "Adv. Quant. Chem.",
    "Accounts of Chemical Research" : "Acc. Chem. Res.",
    "The Annals of Statistics" : "Ann. Stat.",
    "Annual Reviews of Physical Chemistry" : "Annu. Rev. Phys. Chem.",
    "The Journal of Chemical Physics" : "J. Chem. Phys.",
    "Journal of Chemical Physics"     : "J. Chem. Phys.",
    "Theoretical Chemistry Accounts"  : "Theor. Chem. Acc.",
    "Journal of Computational Chemistry" : "J. Comput. Chem.",
    "Physical Chemistry Chemical Physics" : "Phys. Chem. Chem. Phys.",
    "Theoretica Chimica Acta" : "Theoret. Chim. Acta",
    "Theor. Chim. Acta" : "Theoret. Chim. Acta",
    "Cell" : "Cell",
    "Chemical Physics Letters" : "Chem. Phys. Lett.",
    "Chemical Physics" : "Chem. Phys.",
    "Chemical Reviews" : "Chem. Rev.",
    "Chemical Science" : "Chem. Sci.",
    "ChemPhysChem" : "ChemPhysChem",
    "Constrained Approximations" : "Constr. Approx.",
    "Central European Journal of Physics" : "Cent. Eur. J. Phys.",
    "International Journal of Quantum Chemistry" : "Int. J. Quant. Chem.",
    "Int. J. Quantum Chem." : "Int. J. Quant. Chem.",
    "International Journal of Modern Physics B" : "Int. J. Mod. Phys. B",
    "International Journal of Radiation Biology" : "Int. J. Radiat. Biol.",
    "Journal of the American Chemical Society" : "J. Am. Chem. Soc.",
    "Journal of Molecular Structure (Theochem)" : "J. Mol. Struct. (Theochem)",
    "Journal of Chemical Education" : "J. Chem. Educ.",
    "Journal of Chemical Theory and Computation" : "J. Chem. Theory Comput.",
    "Journal of Physical Chemistry A" : "J. Phys. Chem. A",
    "Journal of Physical Chemistry B" : "J. Phys. Chem. B",
    "Journal of Physical Chemistry C" : "J. Phys. Chem. C",
    "Journal of Physical Chemistry Letters" : "J. Phys. Chem. Lett.",
    "Journal of Physics B: Atomic, Molecular and Optical Physics" : "J. Phys. B: At., Mol. Opt. Phys.",
    "Nature Reviews Genetics" : "Nat. Rev. Gen.",
    "Nature Communications" : "Nat. Commun.",
    "Mathematics Annals" : "Math. Ann.",
    "Molecules" : "Molecules",
    "Comput. Mater. Sci." : "Comput. Mater. Sci.",
    "Molecular Physics" : "Mol. Phys.",
    "Optics and Spectroscopy" : "Opt. Spectrosc.",
    "Physical Review A" : "Phys. Rev. A",
    "Physical Review B" : "Phys. Rev. B",
    "Physical Review C" : "Phys. Rev. C",
    "Physical Review Letters" : "Phys. Rev. Lett.",
    "Physkalische Zeitschrift der Sowjetunion" : "Phys. Zeit. der Sowjetunion",
    "Proceedings of the National Academy of Sciences" : "Proc. Natl Acad. Sci.",
    "Reviews of Modern Physics" : "Rev. Mod. Phys.",
    "WIREs Computational Molecular Science" : "WIREs Comput. Mol. Sci.",
    "Wiley Interdisciplinary Reviews: Computational Molecular Science" : "Wiley Interdiscip. Rev.: Comput. Mol. Sci."
}

accepted_abbreviations = set([journal_abbreviations[x] for x in journal_abbreviations])

def keyify(key):
    out = key

    # first replace umlauted vowels by their base plus e
    for uml, rep in [ ('{\\\"a}', 'ae'), ('{\\\"o}', 'oe'), ('{\\\"u}', 'ue')]:
        out = out.replace(uml, rep)

    # finally strip out punctuation and whitespace
    return "".join(filter(lambda x: x not in (string.punctuation+string.whitespace), out))

# Checks for and returns abbreviated journal name
def abbreviator(journal):
    out = ""
    if journal in journal_abbreviations:
        out = journal_abbreviations[journal]
    elif journal in accepted_abbreviations:
        out = journal
    else:
        raise JournalError(journal)

    return out

def surname(author):
    if ',' in author:
        return author.split(',')[0]
    else:
        return author.split()[-1]

def file_search_replace(filename, replace):
    lines = []
    with open(filename, "r") as f:
        for line in f:
            lineout = line
            for rep in replace:
                lineout = lineout.replace(rep, replace[rep])
            lines.append(lineout.rstrip())

    with open("{0}.tmp".format(filename), "w") as f:
        for line in lines:
            print(line, file=f)

if __name__ == "__main__":
    ap = argparse.ArgumentParser(usage='Given a bibfile, processes it and overwrites the cite key to match RefBase')
    ap.add_argument("bibfile", help="file to read")
    ap.add_argument("texfiles", nargs='*', help="replace keys in existing files")
    ap.add_argument("--inplace", "-i", action='store_true', help="update bibfile in place")

    args = ap.parse_args()

    parser = bibtexparser.bparser.BibTexParser(common_strings=True)
    parser.homogenise_fields = True

    with open(args.bibfile) as bibfile:
        bibdb = bibtexparser.load(bibfile, parser)

    # start a new database
    outdb = bibtexparser.bibdatabase.BibDatabase()

    idmap = {}

    for i, paper in enumerate(bibdb.entries):
        try:
            if paper['ENTRYTYPE'].lower() == "article":
                newentry = {}

                # if the following entries are found, copy them over without modification
                for label in ['title', 'number', 'ENTRYTYPE', 'volume', 'year', 'number', 'pages', 'doi' ]:
                    if label in paper:
                        newentry[label] = paper[label]

                # remove newlines from author list
                newentry['author'] = paper['author'].replace('\n',' ')

                # handle abbreviated journal name separately
                if 'journal' in paper:
                    newentry['journal'] = abbreviator(paper['journal'])

                # build ID directly
                firstauthor = surname(newentry['author'].split(' and ')[0])
                yr2 = newentry['year'][2:4]
                jo = abbreviator(newentry['journal'])
                pg = "p" + newentry['pages'].split('-')[0]
                vol = newentry['volume']

                newentry['ID'] = keyify(firstauthor + yr2 + jo + vol + pg)

                old, new = paper['ID'], newentry['ID']
                if (old != new):
                    idmap[old] = new

                outdb.entries.append(newentry)
            else:
                outdb.entries.append(paper)
        except JournalError:
            raise
        except Exception as e:
            print(e)
            print(paper['ID'])
            print("Article processing failed for entry %d. Including the unprocessed entry." % (i+1),
                    file=sys.stderr)
            outdb.entries.append(paper)

    writer = bibtexparser.bwriter.BibTexWriter()
    with open(args.bibfile + ".tmp", "w") as f:
        f.write(writer.write(outdb))

    if args.inplace:
        os.rename(args.bibfile + ".tmp", args.bibfile)

    for filename in args.texfiles:
        print("Replacing in file {0} -> {0}.tmp".format(filename), file=sys.stderr)
        file_search_replace(filename, idmap)
