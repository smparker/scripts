#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import scipy.optimize as spo
import scipy.spatial.distance as spd
import argparse

## Standalone script to interpolate between two xyz files.
## Interpolation between structures
## is performed using Halgren and Lipscomb's linear synchronous transit.

class Structure(object):
    """Coordinates and symbols from xyz"""
    def __init__(self, symbols, coords):
        self.symbols = symbols
        self.coord = coords

    def print(self):
        natoms = len(self.symbols)
        print("%d" % natoms)
        print()
        for i in range(natoms):
            print("%3s %22.16f %22.16f %22.16f" %
                (self.symbols[i], self.coord[i,0], self.coord[i,1], self.coord[i,2]))

    @classmethod
    def from_xyz(cls, filename):
        """generate structure from .xyz file"""
        symbols = []
        fh = open(filename,"r")

        natoms = int(fh.readline())
        comment = fh.readline()

        coords = np.zeros([natoms,3])

        for iatom in range(natoms):
            tmp = fh.readline().split()

            symb = str(tmp[0]).lower()
            symbols.append(symb)

            coords[iatom,:] = [ float(x) for x in tmp[1:4] ]

        return cls(symbols, coords)

def distance_matrix(coordw):
    """compute r_ij for all atoms i < j"""
    coord = np.reshape(coordw, (coordw.size//3,3))
    return spd.pdist(coord, 'euclidean')

def transit_metric(coordw, interp_coord, interp_distance, zeta = 1.0e-6):
    """computes metric for LST"""
    coord = np.reshape(coordw, (coordw.size//3,3))

    distance = distance_matrix(coord)
    natoms = coord.shape[0]

    coord_diff = coord - interp_coord
    dist_diff = distance - interp_distance

    metric = np.sum(dist_diff**2/(interp_distance**4))
    metric += zeta * np.vdot(coord_diff, coord_diff)

    return metric

def transit_gradient(coordw, interp_coord, interp_distance, zeta = 1.0e-6):
    """computes gradient of metric for LST"""
    coord = np.reshape(coordw, (coordw.size//3,3))

    distance = distance_matrix(coord)
    natoms = coord.shape[0]

    # initialize output
    gradient = 2.0 * zeta * (coord - interp_coord.reshape(coord.shape))

    base_grad = 2.0 * (distance-interp_distance)/(distance*interp_distance**4)
    # convert to squareform
    base_grad = spd.squareform(base_grad)

    flat_grad = np.sum(base_grad, axis=1)
    for i in range(natoms):
        gradient[i,:] += coord[i,:] * flat_grad[i]

    gradient -= np.dot(base_grad, coord)
    return np.reshape(gradient, -1)

def lst(start, end, f, maxiter=500, tol=1e-8):
    """performs LST interpolation between provided endpoints"""
    # check for quick return
    if (f == 0.0):
        return start
    elif (f == 1.0):
        return end

    # first linearly interpolate coordinates
    interp_coord = (1.0 - f) * start + f * end

    coord = np.zeros(interp_coord.shape)
    coord[:,:] = interp_coord[:,:]

    interp_distance = (1.0-f)*distance_matrix(start) + f*distance_matrix(end)
    distance = distance_matrix(coord)

    # Using scipy's minimizer to minimize
    res = spo.minimize(transit_metric, coord, args=(interp_coord, interp_distance),
                       jac=transit_gradient,tol=tol,options={'maxiter':maxiter})
    out = res.x

    return np.reshape(out, coord.shape)

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description = 'Linear Synchronous Transit between two .xyz files')

    ap.add_argument('start', metavar='start', help='Starting configuration', type=str)
    ap.add_argument('end', metavar='end', help='Final configuration', type=str)
    ap.add_argument('frac', metavar='frac', help='Interpolation fraction', type=float)

    ap.add_argument('-t', '--tol', metavar='tol', help='Optimization tolerance', default=1.0e-8, type=float)
    ap.add_argument('-m', '--maxiter', metavar='maxiter', help='Maximum Iteration of the minimizer', default=500, type=int)

    args = ap.parse_args()

    start = Structure.from_xyz(args.start)
    end = Structure.from_xyz(args.end)

    fcoord = args.frac * end.coord + (1 - args.frac) * start.coord
    fdistance = distance_matrix(fcoord)
    fcoord = lst(start.coord, end.coord, args.frac, tol=args.tol, maxiter=args.maxiter)

    out = Structure(start.symbols, fcoord)
    out.print()
