#!/usr/bin/env python

from __future__ import print_function, division

import argparse
import sys

def fd_coeffs(npoints):
    if npoints < 2:
        raise Exception("At least two points needed for finite difference")
    if npoints % 2 == 1:
        raise Exception("Finite difference for first derivatives take an even number of points")
    order = (npoints//2) - 1

    coeffs = [[ -0.5, 0.5 ],
              [ 1.0/12.0, -2.0/3.0, 2.0/3.0, -1.0/12.0 ],
              [-1.0/60.0, 3.0/20.0, -0.75, 0.75, -3.0/20.0, 1.0/60.0],
              [1.0/280.0, -4.0/105.0, 0.2, -0.8, 0.8, -0.2, 4.0/105.0, -1.0/280.0]
             ][order]
    return coeffs

def differentiate(data, step):
    coeffs = fd_coeffs(len(data))
    dfdx = sum([fx*cx for fx, cx in zip(data, coeffs)])
    return dfdx / step

def differentiate_all_order(data, step):
    npoints = len(data)
    if len(data) % 2 == 1:
        raise Exception("Finite difference formula needs even number of points")

    nstencil = npoints//2

    out = []
    for istencil in range(nstencil):
        order = istencil + 1
        padding = nstencil - order
        dfdx = differentiate(data[padding:padding+2*order], step)
        out.append((2*order, dfdx))

    return out

if __name__ == "__main__":
    ap = argparse.ArgumentParser(description="Finite difference formula computer")
    ap.add_argument("step", type=float, help="Stepsize for finite difference")
    ap.add_argument("data", type=float, nargs='+', help="Data to apply finite difference stencils to")

    args = ap.parse_args()

    for order, dfdx in differentiate_all_order(args.data, args.step):
        print("%d-point stencil: df/dx = %22.12f" % (order, dfdx))
