#!/usr/bin/env python

import scipy.interpolate as interp
import numpy as np
import math as m
import sys
import os

def usage():
  print "Usage:"
  print "  %s <filename> <nx> <ny>" % sys.argv[0]
  print
  print "filename -- file to be interpolated"
  print "nx       -- number of points to sample in x-direction"
  print "ny       -- number of points to sample in y-direction"
  
if( not (len(sys.argv)==4) ):
  usage()
  if (len(sys.argv) == 1 ) :
    exit()
  else :
    exit("Improper number of arguments!")

file = sys.argv[1]
nx = sys.argv[2]
ny = sys.argv[3]

f = open(file)
raw = f.readlines()
f.close()

for i in reversed(range(len(raw))):
  if(raw[i]=='\n'):
    raw.pop(i)

xvalues = np.zeros([len(raw)])
yvalues = np.zeros([len(raw)])
zvalues = np.zeros([len(raw)])
points = np.zeros([len(raw), 2])

for i in range(len(raw)):
  tmp = raw[i].split()
  xvalues[i] = float(tmp[0])
  points[i, 0] = float(tmp[0])
  yvalues[i] = float(tmp[1])
  points[i, 1] = float(tmp[1])
  zvalues[i] = float(tmp[2])

smoothed = interp.CloughTocher2DInterpolator(points, zvalues, tol=1.0e-12, maxiter=100)

xx = np.linspace(xvalues.min(), xvalues.max(), nx)
yy = np.linspace(yvalues.min(), yvalues.max(), ny)

for X in xx:
  for Y in yy:
    print("%10f %10f %10f") % (X, Y, smoothed([[X, Y]]))
  print
