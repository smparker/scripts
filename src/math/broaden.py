#!/usr/bin/env python

from __future__ import print_function

import numpy as np
import math as m

# for conversions, all inputs need to be in Hartree, then this scale works
au2GM = 0.012527369857883948
osc2cs = 0.14404397581568953
H2eV = 27.211385
nm2H = 45.56335

class Spectrum(object):
    def __init__(self, energies, strengths):
        self.energies = energies
        self.strengths = strengths

    @classmethod
    def from_file(cls, filename, ien, ist):
        energies = []
        strengths = []
        with open(filename, "r") as f:
            for line in f:
                if line.lstrip()[0]=="#": continue
                tmp = line.split()
                energies.append(float(tmp[ien-1]))
                strengths.append(float(tmp[ist-1]))

        return cls(np.array(energies), np.array(strengths))

def broaden_opa(spectrum, freqs, gamma):
      """freqs should be an iterator that returns the values at which the broadened spectra are desired"""
      lorentz = np.zeros(len(spectrum.energies))
      g2 = gamma * gamma
      strengths = osc2cs * spectrum.strengths * gamma / m.pi # missing a cross section scale
      for w in freqs:
          lorentz = strengths / ((w - spectrum.energies)**2 + g2)
          sigma = sum(lorentz)
          yield (w*H2eV, nm2H/w, sigma)

def broaden_tpa(spectrum, freqs, gamma):
      """freqs should be an iterator that returns the values at which the broadened spectra are desired"""
      lorentz = np.zeros(len(spectrum.energies))
      g2 = gamma * gamma
      strengths = au2GM * spectrum.strengths * gamma / m.pi
      for w in freqs:
          lorentz = strengths / ((2.0 * w - spectrum.energies)**2 + g2)
          sigma = sum(lorentz) * w**2
          yield (w*H2eV, nm2H/w, sigma)

def main():
    import argparse

    ap = argparse.ArgumentParser(description="Broadens absorption strengths and converts to appropriate units " +
        "for one-photon and two-photon spectra. All energies and absorption strengths read from file must be in " +
        "Hartrees. Broadens using a Lorenztian with user defined half-width-at-half-maximum. The output spectra " +
        "is in eV and nm.")

    ap.add_argument("spectrum", help="file containing spectrum")
    ap.add_argument("-o", "--opa", help="broaden a one-photon absorption spectrum (default)", action="store_true")
    ap.add_argument("-t", "--tpa", help="broaden a two-photon absorption spectrum", action="store_true")
    ap.add_argument("-g", "--gamma", help="half-width-at-half-maximum in eV (%(default)f)", type=float, default=0.1)
    ap.add_argument("-p", "--points", help="number of points to sample (%(default)d)", type=int, default=1000)
    ap.add_argument("-r", "--range", nargs=2, help="plot range in eV (fits all excitations)", type=float, default=None)
    ap.add_argument("-w", "--omega", help="index of excitation energy in file (%(default)d)", type=int, default=1)
    ap.add_argument("-d", "--delta", help="index of transition strength in file (%(default)d)", type=int, default=2)

    args = ap.parse_args()

    if (not args.opa) and (not args.tpa): # default to opa
        args.opa = True
    elif args.opa == args.tpa: # both are specified
        raise Exception("Only one of --opa and --tpa can be specified at a time")

    spec = Spectrum.from_file(args.spectrum, args.omega, args.delta)

    args.gamma /= H2eV

    if args.range is None:
        # default to big enough window to fit at least 98% of all broadened spectra
        enclose = 0.98 # 98 %
        edge = args.gamma * m.tan(0.5 * m.pi * enclose)

        left = max(min(spec.energies) - edge, 1.0e-16)
        right = (max(spec.energies) + edge)

        if args.tpa: left, right = 0.5*left, 0.5*right

        args.range = (left, right)
    else:
        args.range = args.range[0] / H2eV, args.range[1] / H2eV

    freqs = np.linspace(args.range[0], args.range[1], args.points, endpoint=True)

    if args.opa:
        broaden = broaden_opa(spec, freqs, args.gamma)
    elif args.tpa:
        broaden = broaden_tpa(spec, freqs, args.gamma)

    for x in broaden:
        print("%18.12f %18.12f %18.12f" % x)

if __name__ == "__main__":
    main()
