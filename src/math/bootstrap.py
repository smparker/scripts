#!/usr/bin/env python3

import argparse
import numpy as np
import scipy
import scipy.interpolate
import scipy.optimize
import os
import json
import time

au2angstrom = 0.529177

#RANDOM_STATE = None

class Timer(object):
    """Convenience class for measuring timing of chunks of code"""

    def __init__(self):
        """Start a new timer"""
        self.last_time = time.time()

    def tick(self):
        """Set a new reference time and return the time since the last tick"""
        lt, self.last_time = self.last_time, time.time()
        return self.last_time - lt

    def tick_print(self, label):
        """Calls tick and automatically prints the output with the given label"""
        out = self.tick()
        print("  %40s: %.4f sec" % (label, out))
        return out

def get_confidence_interval(data, CI=0.95):
    assert CI > 0.0 and CI < 1.0
    pitch = int(len(data) * CI)

    nwindows = len(data) - pitch
    data.sort()
    spans = [ abs(data[i] - data[i+pitch]) for i in range(nwindows) ]
    start = np.argmin(spans)

    return data[start], data[start+pitch]

def bootstrap_values(values, nbootstraps=10000, random=np.random):
    """Bootstraps on individual values (bad for time series)"""
    fac = 1.0/len(values)
    out = [ ]
    #random.set_state(RANDOM_STATE)
    for iboot in range(nbootstraps):
        sample_vals = random.choice(values, len(values))
        mean = np.sum(sample_vals) * fac
        out.append(mean)

    return out

def bootstrap_values_confidence_interval(values, nbootstraps=10000, CI=0.95, random=np.random):
    sampled_data = bootstrap_values(values, nbootstraps=nbootstraps, random=np.random)
    return get_confidence_interval(sampled_data, CI)

def record_value_bootstrap(filename, values, nbootstraps=10000, CI=0.95, random=np.random):
    #random.set_state(RANDOM_STATE)
    fac = 1.0 / len(values)
    mean = np.sum(values) * fac
    ci = bootstrap_values_confidence_interval(values, nbootstraps=nbootstraps, CI=CI, random=random)
    with open(filename, "w") as f:
        print("mean: {:12.6f}".format(mean), file=f)
        print("lower: {:12.6f}".format(ci[0]), file=f)
        print("upper: {:12.6f}".format(ci[1]), file=f)
        print("interval: {:8.4f}".format(CI), file=f)
        print("samples: {:d}".format(nbootstraps), file=f)

def bootstrap_reducer(reducer, trace_funcs, nbootstraps=10000, random=np.random):
    fac = 1.0/len(trace_funcs)
    out = [ ]
    #random.set_state(RANDOM_STATE)
    for iboot in range(nbootstraps):
        sample_funcs = random.choice(trace_funcs, len(trace_funcs))
        def sample_mean(t):
            return fac * sum( (s(t) for s in sample_funcs) )
        def sample_mean_der(t):
            return fac * sum( (s(t, 1) for s in sample_funcs) )
        reduced = reducer(sample_mean, sample_mean_der)
        if reduced is not None:
            out.append(reduced)
        else:
            for t in np.linspace(0, 1000*40, 100):
               print("{t:12.6f} {samp:12.6f}".format(t=t, samp=sample_mean(t)))
            raise Exception("should not have happened")
    if not out:
        raise Exception("No reductions succeeded!")
    return out

def bootstrap_reducer_confidence_interval(reducer, trace_funcs, nbootstraps=10000, CI=0.95, random=np.random):
    sampled_data = bootstrap_reducer(reducer, trace_funcs, nbootstraps=nbootstraps, random=random)
    return get_confidence_interval(sampled_data, CI)

def record_reduction(filename, reducer, trace_funcs, nbootstraps=10000, CI=0.95, random=np.random):
    #random.set_state(RANDOM_STATE)
    fac = 1.0 / len(trace_funcs)
    def mean(t):
        return fac * sum( (s(t) for s in trace_funcs) )
    def mean_der(t):
        return fac * sum( (s(t, 1) for s in trace_funcs) )
    reduced_mean = reducer(mean, mean_der)
    reduced_ci = bootstrap_reducer_confidence_interval(reducer, trace_funcs, nbootstraps=nbootstraps, random=random)
    with open(filename, "w") as f:
        print("mean: {:12.6f}".format(reduced_mean), file=f)
        print("lower: {:12.6f}".format(reduced_ci[0]), file=f)
        print("upper: {:12.6f}".format(reduced_ci[1]), file=f)
        print("interval: {:8.4f}".format(CI), file=f)
        print("samples: {:d}".format(nbootstraps), file=f)

def timeseries_mean(trace_funcs, time_points):
    mean = np.zeros(len(time_points))
    for i, t in enumerate(time_points):
        mean[i] = sum( ( tfunc(t) for tfunc in trace_funcs ) )
    mean *= 1.0/len(trace_funcs)
    return mean

def bootstrap_timeseries(trace_funcs, time_points, nbootstraps=10000, random=np.random):
    #random.set_state(RANDOM_STATE)
    out = []
    data = np.zeros([len(trace_funcs), len(time_points)])
    for itrace in range(len(trace_funcs)):
        for i, t in enumerate(time_points):
            data[itrace, i] = trace_funcs[itrace](t)

    for iboot in range(nbootstraps):
        boot_means = np.zeros(len(time_points))
        boot_sample = random.choice(len(trace_funcs), len(trace_funcs))
        boot_means += sum( (data[s,:] for s in boot_sample) )
        boot_means *= 1.0/len(trace_funcs)
        out.append(boot_means)
    return out

def bootstrap_timeseries_confidence_interval(trace_funcs, time_points, nbootstraps=10000, CI=0.95, random=np.random):
    nlog = len(trace_funcs)
    lower = np.zeros(len(time_points))
    upper = np.zeros(len(time_points))

    timeseries = bootstrap_timeseries(trace_funcs, time_points, nbootstraps=nbootstraps, random=random)
    for i, t in enumerate(timepoints):
        this_data = [ timeseries[j][i] for j in range(len(timeseries)) ]
        lower[i], upper[i] = get_confidence_interval(this_data, CI)
    return lower, upper

def record_time_series(filename, trace_funcs, time_points, nbootstraps=10000, CI=0.95, random=np.random):
    mean = timeseries_mean(trace_funcs, time_points)
    ci = bootstrap_timeseries_confidence_interval(trace_funcs, time_points, nbootstraps=nbootstraps, CI=CI, random=random)
    with open(filename, "w") as f:
        print("#Confidence Intervals: {:8.4f}".format(CI), file=f)
        print("#Bootstrap Samples: {:d}".format(nbootstraps), file=f)
        print("#{:12s} {:12s} {:12s} {:12s}".format("time", "mean", "lower", "upper"), file=f)
        for i, t in enumerate(time_points):
            mean_t = mean[i]
            l, r = ci[0][i], ci[1][i]
            print("{time:12.6f} {mean_t:12.6f} {l:12.6f} {r:12.6f}".format(time=t, mean_t=mean_t, l=l, r=r), file=f)

def interpolate_data(log, keyfunc):
    nsteps = len(log)
    times = np.zeros(nsteps)
    data = np.zeros(nsteps)

    for i in range(nsteps):
        times[i] = log[i]['time']
        data[i] = keyfunc(log[i])

    return scipy.interpolate.interp1d(times, data, kind='cubic', assume_sorted=True)

def interpolate_populations(log, state):
    def keyfunc(snapshot):
        return snapshot['population_el'][state]
    return interpolate_data(log, keyfunc)

if __name__ == "__main__":
    parser = argparse.ArgumentParser("fssh-analyzer")
    parser.add_argument("logs", nargs='*', help="log files to use to generate results")
    parser.add_argument("--seed", "-s", default=None, help="random seed")
    parser.add_argument("--nbootstraps", "-n", default=10000, help="number of bootstrap samples")
    parser.add_argument("--ci", "-c", default=0.95, help="confidence interval")
    parser.add_argument("--pre", "-p", default=None, help="prefix")

    args = parser.parse_args()

    random = np.random.RandomState(args.seed)
    random_state = random.get_state()
    #RANDOM_STATE = random_state

    random_tester(random, random_state)

    timer = Timer()

    logs = [ json.load(open(l, 'r')) for l in args.logs ]
    timer.tick_print("load logs")

    if not logs:
        print("No logs input. What gives?")
        quit()

    start_times = [ l[0]['time'] for l in logs ]
    end_times = [ l[-1]['time'] for l in logs ]
    if False:
        for i in sorted(range(len(logs)), key=lambda x: end_times[x]):
            print("{} {}".format(args.logs[i], end_times[i]))
        quit()

    window = max(start_times), min(end_times)
    shortest = logs[end_times.index(window[1])]
    timepoints = np.array([ x['time'] for x in shortest ])
    #timepoints = np.linspace(window[0], window[1], 1000)

    if args.pre is None:
        common = os.path.commonprefix(args.logs)
        args.pre = common.replace("/","-")

    def time_series(filename, funcs):
        record_time_series("{}-{}".format(args.pre, filename), funcs, timepoints, nbootstraps=args.nbootstraps, CI=args.ci, random=random)

    def reduction(filename, reducer, funcs):
        record_reduction("{}-{}".format(args.pre, filename), reducer, funcs, nbootstraps=args.nbootstraps, CI=args.ci, random=random)

    timer.tick()
    pop_funcs = [ [ interpolate_populations(l, i) for l in logs ] for i in range(3) ]
    timer.tick_print("population functions")
    swarm_funcs = [ [ interpolate_swarm_pop(l, i) for l in logs ] for i in range(3) ]
    timer.tick_print("swarm functions")
    accum_funcs = [ cumulative_s0(l) for l in logs ]
    timer.tick_print("cumulative function")
    bond_CC_funcs = [ interpolate_bond(l, 5, 7) for l in logs ]
    timer.tick_print("bond CC functions")
    bond_CO_funcs = [ interpolate_bond(l, 7, 8) for l in logs ]
    timer.tick_print("bond CO functions")
    bond_CMe_funcs = [ interpolate_bond(l, 5, 6) for l in logs ]
    timer.tick_print("bond CMe functions")
    puck_funcs = [ interpolate_puck(l, [1, 3, 4, 5, 7, 9]) for l in logs ]
    timer.tick_print("puck function")

    time_series("populations.0", pop_funcs[0])
    timer.tick_print("ground state populations")
    time_series("populations.1", pop_funcs[1])
    timer.tick_print("1st excited state populations")
    time_series("populations.2", pop_funcs[2])
    timer.tick_print("2nd excited state populations")

    time_series("swarm.0", swarm_funcs[0])
    timer.tick_print("ground state swarm")
    time_series("swarm.1", swarm_funcs[1])
    timer.tick_print("1st excited state swarm")
    time_series("swarm.2", swarm_funcs[2])
    timer.tick_print("2nd excited state swarm")

    time_series("s0_cum", accum_funcs)
    timer.tick_print("s0 cum")

    time_series("CC", bond_CC_funcs)
    timer.tick_print("CC")
    time_series("CO", bond_CO_funcs)
    timer.tick_print("CO")
    time_series("CMe", bond_CMe_funcs)
    timer.tick_print("CMe")
    time_series("pucker", puck_funcs)
    timer.tick_print("pucker")

    #reduction("s2halflife", halflife_reducer, swarm_funcs[2])
    #timer.tick_print("S2 half-life")
